// tạo array chứa mã màu
// render các button chứa màu
// lấy id của button để tra ra index màu
// gắn index màu vào căn nhà


const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

let renderColorList = () => {
    let content = "";
    let contentHTML = "";
    colorList.forEach((color,index) => {
        console.log(color,index);
        content = `
        <div class="color-button ${color}" onclick="changeHouseColor(${index})"></div>
        `
        contentHTML += content;
        document.getElementById("colorContainer").innerHTML = contentHTML;
    })
}
renderColorList();
let changeHouseColor = (colorIndex) => {
    document.getElementById("house").classList.add(`${colorList[colorIndex]}`);
};